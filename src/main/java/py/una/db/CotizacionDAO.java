package py.una.db;

import java.sql.Connection;

import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import py.una.entidad.Cotizacion;



public class CotizacionDAO {
	
	
	
	public List<Cotizacion> select(){
		String query = "SELECT * FROM COTIZACION";
		List<Cotizacion> listaCotizaciones = new ArrayList<Cotizacion>();
		Connection conn = null;
		
		
		try {
			conn = DB.connect();
			ResultSet result = conn.createStatement().executeQuery(query);
			
			while(result.next()) {
				Cotizacion tmp = new Cotizacion();
				tmp.setId(result.getInt(1));
				tmp.setNombreSucursal(result.getString(2));
				tmp.setGuaraniesCompra(result.getDouble(3));
				tmp.setGuaraniesVenta(result.getDouble(4));
				
				listaCotizaciones.add(tmp);
			}
		}catch(SQLException e) {
			System.out.println("Error ejecutando el select: " + e.getMessage());
		}finally  {
        	try{
        		conn.close();
        	}catch(Exception ef){
        		System.out.println("No se pudo cerrar la conexion a BD: "+ ef.getMessage());
        	}
        }
		
		return listaCotizaciones;
	}
	
	
	public List<Cotizacion> SelectById(int id){
		String SQL = "SELECT * FROM persona WHERE id = ? ";
		
		List<Cotizacion> lista = new ArrayList<Cotizacion>();
		
		Connection conn = null; 
        try 
        {
        	conn = DB.connect();
        	PreparedStatement pstmt = conn.prepareStatement(SQL);
        	pstmt.setInt(1, id);
        	
        	ResultSet result = pstmt.executeQuery();

        	while(result.next()) {
        		Cotizacion  tmp = new Cotizacion();
        		tmp.setId(result.getInt(1));
				tmp.setNombreSucursal(result.getString(2));
				tmp.setGuaraniesCompra(result.getDouble(3));
				tmp.setGuaraniesVenta(result.getDouble(4));
				
        		lista.add(tmp);
        	}
        	
        } catch (SQLException ex) {
            System.out.println("Error en la seleccion: " + ex.getMessage());
        }
        finally  {
        	try{
        		conn.close();
        	}catch(Exception ef){
        		System.out.println("No se pudo cerrar la conexion a BD: "+ ef.getMessage());
        	}
        }
		return lista;
	}
	
	
	public int insertar(Cotizacion c) throws SQLException {
		
		String SQL = "INSERT INTO cotizacion(id, nombre,guaranies_compra,guaranies_venta) "
                + "VALUES(?,?,?,?)";
		int id = 0;
		Connection conn = null;
		try 
        {
        	conn = DB.connect();
        	PreparedStatement pstmt = conn.prepareStatement(SQL, Statement.RETURN_GENERATED_KEYS);
            pstmt.setInt(1, c.getId());
            pstmt.setString(2, c.getNombreSucursal());
            pstmt.setDouble(3, c.getGuaraniesCompra());
            pstmt.setDouble(4, c.getGuaraniesVenta());
            
 
            int affectedRows = pstmt.executeUpdate();
            // check the affected rows 
            if (affectedRows > 0) {
                // get the ID back
                try (ResultSet rs = pstmt.getGeneratedKeys()) {
                    if (rs.next()) {
                        id = rs.getInt(1);
                    }
                } catch (SQLException ex) {
                    System.out.println(ex.getMessage());
                }
            }
        } catch (SQLException ex) {
            System.out.println("Error en la insercion: " + ex.getMessage());
        }
        finally  {
        	try{
        		conn.close();
        	}catch(Exception ef){
        		System.out.println("No se pudo cerrar la conexion a BD: "+ ef.getMessage());
        	}
        }
		
		return id;
	}
	
	public int actualizar(Cotizacion c) throws SQLException {

        String SQL = "UPDATE cotizacion SET nombre = ? , guaranies_compra = ? , guaranies_venta = ? WHERE id = ? ";
   
        int id = 0;
        Connection conn = null;
     
        try 
        {
        	conn = DB.connect();
        	PreparedStatement pstmt = conn.prepareStatement(SQL, Statement.RETURN_GENERATED_KEYS);
             pstmt.setString(1, c.getNombreSucursal());
             pstmt.setDouble(2, c.getGuaraniesCompra());
             pstmt.setDouble(3, c.getGuaraniesVenta());
             pstmt.setInt(4, c.getId());
            int affectedRows = pstmt.executeUpdate();
            // check the affected rows 
            if (affectedRows > 0) {
                // get the ID back
                try (ResultSet rs = pstmt.getGeneratedKeys()) {
                    if (rs.next()) {
                        id = rs.getInt(1);
                    }
                } catch (SQLException ex) {
                    System.out.println(ex.getMessage());
                }
            }
        } catch (SQLException ex) {
            System.out.println("Error en la actualizacion: " + ex.getMessage());
        }
        finally  {
        	try{
        		conn.close();
        	}catch(Exception ef){
        		System.out.println("No se pudo cerrar la conexion a BD: "+ ef.getMessage());
        	}
        }
        return id;
    }
	
	
	public int borrar(int id_cotizacion) throws SQLException {

        String SQL = "DELETE FROM cotizacion WHERE id = ? ";
 
        int id = 0;
        Connection conn = null;
        
        try 
        {
        	conn = DB.connect();
        	PreparedStatement pstmt = conn.prepareStatement(SQL);
            pstmt.setInt(1, id_cotizacion);
 
            int affectedRows = pstmt.executeUpdate();
            // check the affected rows 
            if (affectedRows > 0) {
                // get the ID back
                try (ResultSet rs = pstmt.getGeneratedKeys()) {
                    if (rs.next()) {
                        id = rs.getInt(1);
                    }
                } catch (SQLException ex) {
                    System.out.println(ex.getMessage());
                }
            }
        } catch (SQLException ex) {
            System.out.println("Error en la eliminación: " + ex.getMessage());
        }
        finally  {
        	try{
        		conn.close();
        	}catch(Exception ef){
        		System.out.println("No se pudo cerrar la conexion a BD: "+ ef.getMessage());
        	}
        }
        return id;
    }
	
}