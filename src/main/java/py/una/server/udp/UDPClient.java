package py.una.server.udp;


import java.io.*;
import java.net.*;

import py.una.entidad.Cotizacion;
import py.una.entidad.CotizacionJSON;

class UDPClient {

    public static void main(String a[]) throws Exception {
    	System.out.println("Cliente id: Doriham Tadeo Russo Ortega - 5018972");
        // Datos necesario
        String direccionServidor = "127.0.0.1";

        if (a.length > 0) {
            direccionServidor = a[0];
        }

        int puertoServidor = 9876;
        
        try {

            BufferedReader inFromUser =
                    new BufferedReader(new InputStreamReader(System.in));

            DatagramSocket clientSocket = new DatagramSocket();
            
            InetAddress IPAddress = InetAddress.getByName(direccionServidor);
            System.out.println("Intentando conectar a = " + IPAddress + ":" + puertoServidor +  " via UDP...");

            byte[] sendData = new byte[1024];
            byte[] receiveData = new byte[1024];

            System.out.print("Ingrese el Id de la sucursal (debe ser numérico): ");
            String strId = inFromUser.readLine();
            int id_sucursal = 0;
            try {
            	id_sucursal = Integer.parseInt(strId);
            }catch(Exception e1) {
            	
            }
            System.out.print("Ingrese el nombre de la sucursal: ");
            String nombre = inFromUser.readLine();
            
            System.out.print("Ingrese el precio de venta: ");
            String strPrecioVenta = inFromUser.readLine();
            double precio_venta = 0;
            try {
            	precio_venta = Double.parseDouble(strPrecioVenta);
            }catch(Exception e1) {
            	
            }
            
            System.out.print("Ingrese el precio de Compra: ");
            String strPrecioCompra = inFromUser.readLine();
            double precio_compra = 0;
            try {
            	precio_compra = Double.parseDouble(strPrecioVenta);
            }catch(Exception e1) {
            	
            }
            
            Cotizacion c = new Cotizacion(id_sucursal , precio_compra , precio_venta , nombre);
            
            String datoPaquete = CotizacionJSON.objetoString(c); 
            sendData = datoPaquete.getBytes();

            System.out.println("Enviar " + datoPaquete + " al servidor. ("+ sendData.length + " bytes)");
            DatagramPacket sendPacket =
                    new DatagramPacket(sendData, sendData.length, IPAddress, puertoServidor);

            clientSocket.send(sendPacket);

            DatagramPacket receivePacket =
                    new DatagramPacket(receiveData, receiveData.length);

            System.out.println("Esperamos si viene la respuesta.");

            //Vamos a hacer una llamada BLOQUEANTE entonces establecemos un timeout maximo de espera
            clientSocket.setSoTimeout(10000);

            try {
                // ESPERAMOS LA RESPUESTA, BLOQUENTE
                clientSocket.receive(receivePacket);

                String respuesta = new String(receivePacket.getData());
                Cotizacion cresp = CotizacionJSON.stringObjeto(respuesta.trim());
                
                InetAddress returnIPAddress = receivePacket.getAddress();
                int port = receivePacket.getPort();
                System.out.println("Respueta desde: " + returnIPAddress + ": " + port);
                
            } catch (SocketTimeoutException ste) {

                System.out.println("TimeOut: El paquete udp se asume perdido.");
            }
            clientSocket.close();
        } catch (UnknownHostException ex) {
            System.err.println(ex);
        } catch (IOException ex) {
            System.err.println(ex);
        }
    }
} 

