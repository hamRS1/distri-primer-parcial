package py.una.server.udp;

import java.io.*;
import java.net.*;

import py.una.db.*;
import py.una.entidad.*;

public class UDPServer {
    public static void main(String[] a){
    	System.out.println("server id: Doriham Tadeo Russo Ortega - 5018972");
        // Variables
        int puertoServidor = 9876;
        CotizacionDAO cdao = new CotizacionDAO();
        
        try {
            //1) Creamos el socket Servidor de Datagramas (UDP)
            DatagramSocket serverSocket = new DatagramSocket(puertoServidor);
			System.out.println("Servidor Sistemas Distribuidos para cotizacion - UDP ");
			
            //2) buffer de datos a enviar y recibir
            byte[] receiveData = new byte[1024];
            byte[] sendData = new byte[1024];

			
            //3) Servidor siempre esperando
            while (true) {

                receiveData = new byte[1024];

                DatagramPacket receivePacket =
                        new DatagramPacket(receiveData, receiveData.length);


                System.out.println("Esperando a algun cliente... ");

                // 4) Receive LLAMADA BLOQUEANTE
                serverSocket.receive(receivePacket);
				
				System.out.println("________________________________________________");
                System.out.println("Aceptamos un paquete");

                // Datos recibidos e Identificamos quien nos envio
                String datoRecibido = new String(receivePacket.getData());
                datoRecibido = datoRecibido.trim();
                System.out.println("DatoRecibido: " + datoRecibido );
                Cotizacion c = CotizacionJSON.stringObjeto(datoRecibido);

                InetAddress IPAddress = receivePacket.getAddress();

                int port = receivePacket.getPort();

                System.out.println("De : " + IPAddress + ":" + port);
                System.out.println("Cotizacion Recibida : " + c.getNombreSucursal() + ", compra: " + c.getGuaraniesCompra() + ", venta: " + c.getGuaraniesVenta());
                
                try {
                	cdao.insertar(c);
                	System.out.println("Cotizacion insertada exitosamente en la Base de datos");
                }catch(Exception e) {
                	System.out.println("Cotizacion NO insertada en la Base de datos, razón: " + e.getLocalizedMessage());
                }
                
                //volvemos a enviar el objeto Cotizacion
                sendData = CotizacionJSON.objetoString(c).getBytes();
                DatagramPacket sendPacket =
                        new DatagramPacket(sendData, sendData.length, IPAddress,port);

                serverSocket.send(sendPacket);
            }

        } catch (Exception ex) {
        	ex.printStackTrace();
            System.exit(1);
        }

    }
}  

