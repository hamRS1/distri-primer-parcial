package py.una.entidad;


public class Cotizacion{
	private int id;
	private double guaraniesVenta;
	private double guaraniesCompra;
	private String nombreSucursal;
	
	public Cotizacion() {
		/*empty constructor*/
	}
	
	public Cotizacion(int id, double guaraniesCompra , double guaraniesVenta , String nombreSucursal) {
		this.id = id;
		this.guaraniesCompra = guaraniesCompra;
		this.guaraniesVenta = guaraniesVenta;
		this.nombreSucursal = nombreSucursal;
	}
	
	
	public double getGuaraniesVenta() {
		return this.guaraniesVenta;
	}
	
	public double getGuaraniesCompra() {
		return this.guaraniesCompra;
	}
	
	
	public void setGuaraniesVenta(double guaraniesVenta) {
		this.guaraniesVenta = guaraniesVenta;
	}
	
	public void setGuaraniesCompra(double guaraniesCompra) {
		this.guaraniesCompra = guaraniesCompra;
	}


	public String getNombreSucursal() {
		return nombreSucursal;
	}


	public void setNombreSucursal(String nombreSucursal) {
		this.nombreSucursal = nombreSucursal;
	}
	
	@Override
	public String toString() {
		return "NombreSucursal : " + this.nombreSucursal + "\nGuaranies Venta: " + 
				this.guaraniesVenta + "\nGuaranies Compra: " + this.guaraniesCompra;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}
}