package py.una.entidad;
import java.util.Iterator;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;


public class CotizacionJSON {
	
	
	/*json object a string*/
	public static String objetoString(Cotizacion c) {
		JSONObject obj = new JSONObject();
		obj.put("id", c.getId());
		obj.put("nombre", c.getNombreSucursal());
		obj.put("precioCompra" , c.getGuaraniesCompra());
		obj.put("precioVenta", c.getGuaraniesVenta());
		
		return obj.toJSONString();
		
	}
	
	/*de json string a object*/
	public static Cotizacion stringObjeto(String str) throws Exception{
		Cotizacion c = new Cotizacion();
		JSONParser parse = new JSONParser();
		
		Object obj = parse.parse(str.trim());
		JSONObject jsonObject = (JSONObject) obj;
		
		long id_long 	= (Long) jsonObject.get("id");
		int id = (int) id_long;
		String nombreSucursal =  (String) jsonObject.get("nombre");
		double guaraniesVenta = (Double) jsonObject.get("precioCompra");
		double guaraniesCompra = (Double) jsonObject.get("precioVenta");
		
		
		c.setId(id);
		c.setNombreSucursal(nombreSucursal);
		c.setGuaraniesCompra(guaraniesCompra);
		c.setGuaraniesVenta(guaraniesVenta);
		
		
		return c;
	}
	/*test class*/
	public static void main(String[] args) throws Exception {
		CotizacionJSON test = new CotizacionJSON();
		
		Cotizacion c = new Cotizacion();
		int x = 32;
		c.setId(1);
		c.setNombreSucursal("Banco X");
		c.setGuaraniesCompra(6700);
		c.setGuaraniesVenta(6200);
		
		String s1 = test.objetoString(c);
		System.out.println(s1);
		
		Cotizacion c2 = test.stringObjeto(s1);
		System.out.println(c2);

		
	}
}