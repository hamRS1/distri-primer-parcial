# Servidor y programa cliente UDP
### Funciones del cliente
* Especifica la cotizacion y envia los datos al servidor
### Funciones del servidor
* Guarda los resultados del cliente y envia el dato que se ha solicitado insertar
### Clases Creadas
* En el paquete db
  - CotizacionDAO.java
  - DB.java
  - TEstcotizacionDAO.java
* En el paquete entidad
  - Cotizacion.java
  - CotizacioJSON.java
* En el paquete Server/udp
  - UDPClient.java
  - UDPServer.java

## Screens
![alt text](./screenshots/Screenshot%202022-09-17%20151439.png)
![alt text](./screenshots/Screenshot%202022-09-17%20151453.png)
